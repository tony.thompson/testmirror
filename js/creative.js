/*
creative.js

This is where main logic for the creative bootstrap template is performed.

Modified by Joshua Clark on 4/10/19.

DHA SDK

Copyright © 2009-2019 United States Government as represented by
the Program Manager of the DHA: Web and Mobile Technology Program Management Office.
All Rights Reserved.

Copyright © 2009-2019 Contributors. All Rights Reserved.

THIS OPEN SOURCE AGREEMENT ("AGREEMENT") DEFINES THE RIGHTS OF USE,
REPRODUCTION, DISTRIBUTION, MODIFICATION AND REDISTRIBUTION OF CERTAIN
COMPUTER SOFTWARE ORIGINALLY RELEASED BY THE UNITED STATES GOVERNMENT
AS REPRESENTED BY THE GOVERNMENT AGENCY LISTED BELOW ("GOVERNMENT AGENCY").
THE UNITED STATES GOVERNMENT, AS REPRESENTED BY GOVERNMENT AGENCY, IS AN
INTENDED THIRD-PARTY BENEFICIARY OF ALL SUBSEQUENT DISTRIBUTIONS OR
REDISTRIBUTIONS OF THE SUBJECT SOFTWARE. ANYONE WHO USES, REPRODUCES,
DISTRIBUTES, MODIFIES OR REDISTRIBUTES THE SUBJECT SOFTWARE, AS DEFINED
HEREIN, OR ANY PART THEREOF, IS, BY THAT ACTION, ACCEPTING IN FULL THE
RESPONSIBILITIES AND OBLIGATIONS CONTAINED IN THIS AGREEMENT.

Government Agency: DHA: Web and Mobile Technology Program Management Office
Government Agency Original Software Designation:
Government Agency Original Software Title:
User Registration Requested. Please send email
with your contact information to: robert.a.kayl.civ@mail.mil
Government Agency Point of Contact for Original Software - Program Manager: robert.a.kayl.civ@mail.mil
 */

(function($) {
  "use strict"; // Start of use strict

  // Move the cards up to show a little dha red bros
  var moveUpCard = function () {
    $(".card").hover(function() {
      $(this).css("box-shadow", "2px 2px 8px #343a40");
    }, function(){
      $(this).css("box-shadow", "none");
    });
  };

  // Perform tasks when document is ready
  var setUrls = function () {
    var locationOrigin = location.origin;

    if (title === "DHA SDK (dev)") {
      locationOrigin += "/DevelopmentEnv";
    }

    // Nav
    $("#aboutNavLink").prop("href", locationOrigin + "/#about");
    $("#packagesNavLink").prop("href", locationOrigin + "/#packages");
    $("#applicationsNavLink").prop("href", locationOrigin + "/#applications");
    $("#continuousIntegrationNavLink").prop("href", locationOrigin + "/#continousIntegration");
    $("#resourcesNavLink").prop("href", locationOrigin + "/#resources");
    $("#wmtApplicationsNavLink").prop("href", locationOrigin + "/wmt-applications.html");
    $("#tutorialNavLink").prop("href", locationOrigin + "/Tutorial/#/");

    $(".homeLink").prop("href", locationOrigin + "/#page-top");
    // set security url
    $("#security").prop("href", locationOrigin + "/TypescriptModules/docs/dha-security/docs/");
    // set Database url
    $("#database").prop("href", locationOrigin + "/TypescriptModules/docs/dha-db/docs/");
    // set Encrytion url
    $("#encryption").prop("href", locationOrigin + "/TypescriptModules/docs/dha-encryption/docs/");
    // set different themes url
    $("#themes").prop("href", locationOrigin + "/TypescriptModules/docs/dha-theme/docs/");
    // set the analytics nav link
    $("#analytics").prop("href", locationOrigin + "/TypescriptModules/docs/dha-analytics/docs/");
    // set Breathing Exercise url
    $("#breathingExercise").prop("href", locationOrigin + "/ReactModules/docs/dha-breathing-exercise/styleguide/");
    // set Graph url
    $("#graph").prop("href", locationOrigin + "/ReactModules/docs/dha-graph/styleguide/");
    // set Slider url
    $("#slider").prop("href", locationOrigin + "/ReactModules/docs/dha-slider/styleguide/");
    // set Assets React docs url
    $("#reactAssets").prop("href", locationOrigin + "/ReactModules/docs/dha-assets/styleguide/");
    // set Assets Typescript docs url
    $("#typescriptAssets").prop("href", locationOrigin + "/TypescriptModules/docs/dha-assets/docs/");
    // set login url
    $("#login").prop("href", locationOrigin + "/ReactModules/docs/dha-login/styleguide/");
    // set simple mood tracker url
    $("#simpleMoodTracker").prop("href", locationOrigin + "/SampleApplicationBuilds/simple-mood-tracker-pwa/build/#/");
    // set simple breathe to relax url
    $("#simpleBreatheToRelax").prop("href", locationOrigin + "/SampleApplicationBuilds/simple-b2r-pwa/build/#/");
    // set the tutorial url
    $("#tutorialPage").prop("href", locationOrigin + "/Tutorial/#/");
    // set the mood tracker app link
    $("#moodTrackerPWA").prop("href", locationOrigin + "/SampleApplicationBuilds/simple-mood-tracker-pwa/build/#/");
    // set the mood tracker app link
    $("#breathToRelaxPWA").prop("href", locationOrigin + "/SampleApplicationBuilds/simple-b2r-pwa/build/#/");
  };

  setUrls();
  moveUpCard();

  // Smooth scrolling using jQuery easing
  $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function () {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: (target.offset().top - 56)
        }, 1000, "easeInOutExpo");
        return false;
      }
    }
  });

  // Closes responsive menu when a scroll trigger link is clicked
  $('.js-scroll-trigger').click(function () {
    $('.navbar-collapse').collapse('hide');
  });

  // Activate scrollspy to add active class to navbar items on scroll
  $('body').scrollspy({
    target: '#mainNav',
    offset: 57
  });

  // Collapse Navbar
  var navbarCollapse = function () {
    if ($("#mainNav").offset().top > 100) {
      $("#mainNav").addClass("navbar-shrink");
    } else {
      $("#mainNav").removeClass("navbar-shrink");
    }
  };
  // Collapse now if page is not at top
  navbarCollapse();
  // Collapse the navbar when page is scrolled
  $(window).scroll(navbarCollapse);

  // Collapse React Modules Page Navbar
  var reactModulesNavbarCollapse = function () {
    if ($("#mainNav").offset().top > 100) {
      $("#mainNav").addClass("navbar-shrink");
    } else {
      $("#mainNav").removeClass("navbar-shrink");
    }
  };
  // Collapse now if page is not at top
  reactModulesNavbarCollapse();
  // Collapse the navbar when page is scrolled
  $(window).scroll(reactModulesNavbarCollapse);

  // Scroll reveal calls
  window.sr = ScrollReveal();

  sr.reveal('.sr-icon-1', {
    delay: 200,
    scale: 0
  });
  sr.reveal('.sr-icon-2', {
    delay: 400,
    scale: 0
  });
  sr.reveal('.sr-icon-3', {
    delay: 600,
    scale: 0
  });
  sr.reveal('.sr-icon-4', {
    delay: 800,
    scale: 0
  });
  sr.reveal('.sr-button', {
    delay: 200,
    distance: '15px',
    origin: 'bottom',
    scale: 0.8
  });
  sr.reveal('.sr-contact-1', {
    delay: 200,
    scale: 0
  });
  sr.reveal('.sr-contact-2', {
    delay: 400,
    scale: 0
  });

  // Magnific popup calls
  $('.popup-gallery').magnificPopup({
    delegate: 'a',
    type: 'image',
    tLoading: 'Loading image #%curr%...',
    mainClass: 'mfp-img-mobile',
    gallery: {
      enabled: false,
      navigateByImgClick: true,
      preload: [0, 1]
    },
    image: {
      tError: '<a href="%url%">The image</a> could not be loaded.'
    }
  });

})(jQuery); // End of use strict

function validateContactForm() {
  var errors = [{name: false}, {email: false}, {message: false}];

  if(!validateContactName()){
    // Name is not valid. Turn on error switch.
    errors[0].name = true;
  }
  if(!validateContactEmail()){
    // Email is not valid. Turn on error switch.
    errors[1].email = true;
  }
  if(!validateContactMessage()){
    // Message is not valid. Turn on error switch.
    errors[2].message = true;
  }
  if(checkForErrors(errors)){
    $('#contactSubmit').removeAttr("data-dismiss").removeAttr("data-toggle").removeAttr("data-target");
    return false;
  }
  else {
    $('#contactSubmit').attr("data-dismiss", "modal").attr("data-toggle", "modal").attr("data-target", "#contactConfirmationModal");
    return true;
  }
}

function checkForErrors (errors) {

  var hasErrors = false;

  for (var key in errors) {
    if((errors[key].name === true) || (errors[key].email === true) || (errors[key].message === true)){
      hasErrors = true;
    }
  }
  return hasErrors;
}

function validateContactName() {
  var contactName = $("#contactName")
  var contactNameValue = contactName.val();

  if(contactNameValue === ""){
    contactName.css('border-color', 'red');
    return false;
  }
  else {
    contactName.css('border-color', '');
    return contactNameValue;
  }
}

function validateContactEmail() {
  var contactEmail = $("#contactEmail");
  var contactEmailValue = contactEmail.val();

  if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(contactEmailValue))
  {
    contactEmail.css('border-color', '');
    return (true)
  }
  else {
    contactEmail.css('border-color', 'red');
    return (false)
  }
}

function validateContactMessage() {
  var contactMessage = $("#contactMessage");
  var contactMessageValue = contactMessage.val();

  if(contactMessageValue === ""){
    contactMessage.css('border-color', 'red');
    return false;
  }
  else {
    contactMessage.css('border-color', '');
    return contactMessageValue;
  }
}
